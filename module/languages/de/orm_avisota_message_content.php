<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:18:52+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['articleFull']['0'] = 'Ganzen Artikel anzeigen';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['articleFull']['1'] = 'Diese Einstellung legt fest dass der gesamte Artikel anstatt des Artikelteasers angezeigt wird.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['articleId']['0']   = 'Artikel';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['articleId']['1']   = 'Bite wählen Sie aus welcher Artikel eingebunden werden soll.';
