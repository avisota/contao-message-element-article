<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T16:04:11+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['articleFull']['0'] = 'Mussar l\'entir artitgel';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['articleFull']['1'] = 'Mussar l\'entir artitgel empè dal teaser da l\'artitgel.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['articleId']['0']   = 'Artitgel';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['articleId']['1']   = 'Tscherna l\'artitgel per includer.';
